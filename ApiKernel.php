<?php

namespace Api;

use Silex\Application;

/**
 * Front Controller
 *
 * @final
 */
final class ApiKernel extends Application
{
    /** @var string The namespace of the config env's class */
    private $configNamespace = 'Api\\Config\\Env\\';


    public function __construct()
    {
        parent::__construct();

        $this->configure();
        $this->init();
    }

    /**
     * Configure the application based on the current app's env.
     */
    protected function configure()
    {
        $env       = getenv('APP_ENV');
        $className = ucwords(strtolower($env));
        $class     = $this->configNamespace . $className;

        if (class_exists($class)) {
            (new $class)->register($this);
        } else {
            $message = "<pre>The class <b>{$className}</b> was not found in the namespaces <i>"
                     . rtrim($this->configNamespace, '\\') . '</i></pre>';

            die($message);
        }
    }

    /**
     * Load the specific bundle.
     */
    private function init()
    {
        $uri = explode('/', $_SERVER['REQUEST_URI']);

        $uri = $route = $uri[1];

        $bundle = ucfirst($uri) . 'Bundle';
        $bundle = "Api\\Src\\{$bundle}\\{$bundle}";

        if (class_exists($bundle)) {
            $this["bundle.{$uri}"] = $this->share(function () use ($bundle, $route) {
                return new $bundle($this, $route);
            });

            $this["bundle.{$uri}"];
        }

        // $this->after(function ($request, $response) {
        //     $response->headers->set('Access-Control-Allow-Origin', '*');
        // });

        $this->error($this['service.error']);
    }
}