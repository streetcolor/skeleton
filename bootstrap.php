<?php

require_once __DIR__ . '/vendor/autoload.php';

date_default_timezone_set('Europe/Paris');

if (!defined('APP_ROOT_PATH')) {
    define('APP_ROOT_PATH', __DIR__);
}

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$app = new Api\ApiKernel;