<?php

namespace Validation;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * @final
 */
final class Validator
{
    protected $engine;

    /** @var \Symfony\Component\Validator\ConstraintViolationList */
    protected $violations;

    /**
     * Constructor
     *
     * @param [type] $engine $app['validator'] aka SF2 component
     * @note: Add Type hinting on $engine
     */
    public function __construct($engine)
    {
        $this->engine     = $engine;
        $this->violations = new ConstraintViolationList;
    }

    /**
    * Validates a set of data
    *
    * @param  mixed      $data
    * @param  mixed|null $groups
    * 
    * @return bool
    */
    public function validate($data, array $groups = null)
    {
        $violations = $this->engine->validate($data, null, $groups);
        
        foreach ($violations as $violation) {
            $this->violations->add($violation);
        }

        return count($this->violations) ? false : true;
    }

    /**
     * Get the errors
     *
     * @return array
     */
    public function getErrors()
    {
        $errors = [];

        foreach ($this->violations as $violation) {
            $property          = ltrim(rtrim($violation->getPropertyPath(), ']'), '[');
            $errors[$property] = $violation->getMessage();
        }

        return $errors;
    }

    /**
     * Get a new violation
     *
     * @return \Validation\Validator
     */
    public function reset()
    {
        $this->violations = new ConstraintViolationList;

        return $this;
    }
}
