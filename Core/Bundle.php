<?php

namespace Api\Core;

use Exception;
use ReflectionClass;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ServiceProviderInterface;

/**
 * Base Bundle.
 *
 * @abstract
 */
abstract class Bundle implements ServiceProviderInterface, ControllerProviderInterface
{
    /** @var array|[] All events related to the bundle */
    protected $events = [];

    /** @var array|[] All providers related to the bundle */
    protected $providers = [];


    /**
     * @param \Silex\Application $app
     * @param string             $mount
     */
    public function __construct(Application $app, $mount = '/')
    {        
        $app->mount($mount, $this);
        $app->register($this);
        
        $this->shareProviders($app);
    }

    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * Uses the reflection API to get the name of the class and its namespace.
     * Dynamically registers all events and the bundle's controller.
     * 
     * @param \Silex\Application $app
     */
    public function register(Application $app)
    {
        $reflectionClass = new ReflectionClass($this);

        $namespace       = $reflectionClass->getNamespaceName();
        $class           = explode('Bundle', $reflectionClass->getShortName())[0];
        $key             = strtolower($class);
        $controller      = "{$namespace}\\Controller\\{$class}Controller";

        $app["bundle.{$key}.events"] = $this->getSharedEvents($app);
        $app["{$key}.controller"]    = $app->share(function($app) use ($controller) {
            return new $controller($app);
        });

        if (method_exists($this, 'config')) {
            $app['config'] = $this->config();
        }
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     * 
     * @param Silex\Application $app
     */
    public function boot(Application $app)
    {
        // 
    }

    /**
     * Retrieve all events of a bundle and instanciate them.
     *
     * @throws \Exception
     * 
     * @return array
     */
    private function getSharedEvents(Application $app)
    {
        $sharedEvents = [];

        if (is_array($this->events)) {
            foreach ($this->events as $type => $events) {
                if (is_array($events)) {
                    foreach ($events as $key => $event) {
                        $dic = null; 

                        if (is_array($event)) {
                            $class = $event[0];
                            $dic   = $event[1];
                        }

                        $pattern = explode('.', $class);

                        if (!empty($pattern[1])) {
                            $class = "\\Api\\Src\\{$pattern[0]}\\Event\\{$pattern[1]}";

                            if (class_exists($class)) {
                                if (count($dic)) {
                                    $services = [];

                                    foreach ($dic as $service) {
                                        $services[$service] = $app[$service];
                                    }

                                    $instance = new $class($services);
                                } else {
                                    $instance = new $class();
                                }

                                $sharedEvents[$type][$key] = $instance;
                            } else {
                                throw new Exception("The event {$class} hasn't been found.");
                            }
                        }
                    }
                }
            }
        }

        return $sharedEvents;
    }

    /**
     * Register all providers.
     *
     * @param  \Silex\Application $app
     *
     * @throws \Exception
     */
    private function shareProviders(Application $app)
    {
        foreach ($this->providers as $provider) {
            if (!empty($provider['name'])) {
                $pattern = explode('.', $provider['name']);

                if (isset($pattern[1])) {
                    $class = "\\Api\\Src\\{$pattern[0]}\\Provider\\{$pattern[1]}";

                    if (class_exists($class)) {
                        $args = !empty($provider['args']) ? $provider['args'] : [];
                        $this->setArgsParameter($args);
                        
                        $app->register(new $class, $args);
                    } else {
                        throw new Exception("The provider {$class} hasn't been found.");
                    }
                }
            }
        }
    }

    private function setArgsParameter(array &$args)
    {
        foreach ($args as $key => &$value) {
            if (is_array($value)) {
                $this->setArgsParameter($value);
            } else if (strpos($value, 'ENV.') === 0) {
                $value = getenv(str_replace('ENV.', '', $value));
            }
        }
    }
}