<?php

namespace Api\Core;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

interface Middleware
{
    public function handle(Request $request, Response $response);
}