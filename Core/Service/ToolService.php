<?php

namespace Api\Core\Service;

class ToolService
{
    /** string */
    const SALT_KEY = '5D8mFmI6LCWn1h9Wd9ckJZZLBuFuSFAsm';


    /**
     * Encrypt a stirng using salt and mcrypt.
     * 
     * @param  string $string
     * 
     * @return string
     */
    public static function encrypt($string)
    {
        $salt = md5(self::SALT_KEY);

        return strtr(
            base64_encode(
                mcrypt_encrypt(
                    MCRYPT_RIJNDAEL_256,
                    $salt,
                    serialize($string),
                    MCRYPT_MODE_CBC,
                    md5($salt)
                )
            ),
            '+/=',
            '-_,'
        );
    }

    /**
     * Decrypt a string using mcrypt and the salt.
     * 
     * @param  string $encrypted
     * 
     * @return mixed
     */
    public static function decrypt($encrypted)
    {
        $salt = md5(self::SALT_KEY);

        return unserialize(
            rtrim(
                mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_256,
                    $salt,
                    base64_decode(strtr($encrypted, '-_,', '+/=')),
                    MCRYPT_MODE_CBC,
                    md5($salt)
                ),
            "\0")
        );
    }

    /**
     * Format a string for the SEO
     * 
     * @param  string $str
     * @param  array  $replace
     * @param  string $delimiter
     * 
     * @return string
     */
    public static function formatSEO($str, array $replace = [], $delimiter = '-')
    {
        if (substr($str, 0, 1) === '/') {
            $str = substr($str, 1);
        }
        
        setlocale(LC_ALL, 'en_US.UTF8');
        
        if (!empty($replace)) {
            $str = str_replace((array)$replace, ' ', $str);
        }
        
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace('/[^a-zA-Z0-9\/_|+ -]/', '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace('/[\/_|+ -]+/', $delimiter, $clean);
        
        return trim('/' . $clean, '-');
   }
}