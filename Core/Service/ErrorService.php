<?php

namespace Api\Core\Service;

use Exception;
use Doctrine\DBAL\DBALException;
use Silex\Application;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Handle all the errors of the app.
 * 
 * @final
 */
final class ErrorService
{
    /** @var \Silex\Application $app */
    private $app;


    /**
     * @param \Silex\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle all exception and errors.
     * 
     * @param  \Exception $e
     * @param  int        $code
     * 
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function handle(Exception $e, $code)
    {
        if ($this->app['debug']) {
            return;
        }

        // @todo add a method to generate to response

        return $this->app->json([
            'status'  => false,
            'code'    => 404,
            'message' => $e->getMessage(),
        ], 200);
    }

    // $app->error(function(\Doctrine\DBAL\DBALException $e, $code) use ($app) {
    //     switch ($e->getErrorCode()) {
    //         case 1062:
    //             $message = 'This entry already exist';
    //             break;
    //         default:
    //             $message = 'An MySql error has occured';
    //             break;
    //     }

    //     // @todo Use HTTP code error
    //     return $app->json([
    //         'status'            => 'error',
    //         'code'              => $code,
    //         'error_sql_code'    => $e->getErrorCode(),
    //         'error_sql_message' => $message,
    //         'message'           => $e->getMessage()
    //     ]);
    // });
}