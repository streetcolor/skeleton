<?php

namespace Api\Core\Service;

use Silex\Application;

class CurrencyService
{
    /** @var \Silex\Application $app */
    private $app;

    protected $rates;

    /**
     * @param \Silex\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        $this->fetchRates();
    }

    /**
     * Convert a price to a given currency
     * 
     * @param  int $amount The amount
     * @param  int $baseCurrency
     * @param  int $toCurrency
     * 
     * @return float
     */
    public function convert($amount, $baseCurrency, $toCurrency)
    {
        if ($baseCurrency == $toCurrency) {
            return $amount;
        }

        foreach ($this->rates as $rate) {
            if ($rate['id_currency_from'] == $baseCurrency && $rate['id_currency_to'] == $toCurrency) {
                $amount = $amount * $rate['currency_rate'];

                return round($amount);
            }
        }
    }

    public function getRate($baseCurrency, $toCurrency)
    {
        foreach ($this->rates as $rate) {
            if ($rate['id_currency_from'] == $baseCurrency && $rate['id_currency_to'] == $toCurrency) {
                return $rate['currency_rate'];
            }
        }

        return 1;
    }

    public function getRates()
    {
        return $this->rates;
    }

    protected function fetchRates()
    {
        $qb = $this->app['orm.em']->createQueryBuilder()
            ->select('currencies_rates')
            ->from('Entity:CurrencyRate', 'currencies_rates');

        foreach ($qb->getQuery()->getResult() as $currency) {
            $this->rates[] = [
                'id_currency_from' => $currency->getIdCurrencyFrom(),
                'id_currency_to'   => $currency->getIdCurrencyTo(),
                'currency_rate'    => $currency->getCurrencyRate(),
            ];
        }
    }
}