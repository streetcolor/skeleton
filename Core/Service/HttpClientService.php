<?php

namespace Api\Core\Service;

use Exception;
use GuzzleHttp\Client;


/**
 * Handle all the errors of the app.
 * 
 * @final
 */
final class HttpClientService
{

    private $method;
    private $endpoint;
    private $type;
    /**
    * @param \Silex\Application $app
    */
    public function __construct($method=null,$endpoint=null, $type=null )
    {
        if(empty($method)){
            throw new Exception('Method not defined');
        }

        if(empty($endpoint)){
            throw new Exception('Endpoint not defined');
        }
        if(empty($type)){
          $type =   getenv('APP_ACCEPTED_CONTENT_TYPE');
        }

        $method = strtolower($method);

        $this->method = $method;
        $this->endpoint = $endpoint;
        $this->type = $type;
    }

    /**
    * Get the client
    * 
    * @return object
    */
    public function handle()
    {
        $client = new Client();
        $headers = [
            'headers' => [
                'content-type' => $this->type,
            ],
            'auth' => [
                'username',
                'password',
            ]
        ];

        return $client->{$this->method}($this->endpoint ,$headers);
    }

}