<?php

namespace Api\Core\Service\Security;

use Silex\Application;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Check the Authorization header.
 * 
 * @final
 */
final class HttpBasicAuthService
{
    /** @var \Silex\Application $app */
    private $app;
    private $basic_auth = "Basic YXJ0c3BlcjpoVGsxZEE3ZmVBdDI=";


    /**
     * @param \Silex\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function handle()
    {
        if (!$this->app['request']->headers->get('Authorization') == $this->basic_auth) {
            throw new AccessDeniedHttpException('Authentication failed.');
        }
    }
}