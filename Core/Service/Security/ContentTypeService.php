<?php

namespace Api\Core\Service\Security;

use Silex\Application;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

/**
 * Check the Content-Type.
 * 
 * @final
 */
final class ContentTypeService
{
    /** @var \Silex\Application $app */
    private $app;
    private $accepted_content_type = array(
        'application/json',
    );


    /**
     * @param \Silex\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @throws \Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException
     */
    public function handle()
    {
        if ($this->app['request']->getMethod() != 'GET') {
            if (in_array($this->app['request']->headers->get('Content-Type'), $this->accepted_content_type)) {
                // Even if the body is null or contains an empty string, it'll be cast to an array
                $body = (array)json_decode($this->app['request']->getContent(), true);

                $this->app['request']->request->replace($body);
            } else {
                throw new NotAcceptableHttpException('Content-Type must be application/json.');
            }
        }
    }
}