<?php

namespace Api\Core\Service;

use Exception;
use Silex\Application;
use \Mandrill;
use \Mandrill_Messages;

/**
 * Sent mail with Mandrill App.
 *
 * Example:
 * $mailer = new MandrillMailerService();
 * $response = $mailer->setFromEmail('name@domain.com')
 *     ->addTo('thibault.colette', 'name@domain.com')
 *     ->setSubject('Thibault test mailer service')
 *     ->setTemplate('test.html')
 *     ->addPlaceholder('RECEIPIENT', 'THIBAULT')
 *     ->sent();
 *
 * Don't forget the use statment:
 *
 * use Api\Core\Service\MandrillMailer\MandrillMailerService;
 * 
 * @final
 */
final class MandrillMailerService
{
    /** These variables are set by the environment file **/
    private $mandrill_api_key;
    private $mandrill;
    private $mandrill_message;

    /** Dynamic variables **/
    private $template_location;
    private $template;
    private $subject;
    private $from_email;
    private $from_name;
    private $to;
    private $placeholders;
    private $merge_vars;
    private $global_merge_vars;
    private $tags;
    private $merge_language;
    private $auto_text;
    private $track_opens;
    private $track_clicks;
    private $subaccount;
    private $google_analytics_domains;
    private $google_analytics_campaign;
    private $plaintext = false;
    private $attachments = array();

    public function __construct($mandrill_api_key, $template_location = null) {
        $this->mandrill_api_key = $mandrill_api_key;

        $this->mandrill = new Mandrill($this->mandrill_api_key);
        $this->mandrill_message = new Mandrill_Messages($this->mandrill);
        $this->template_location = $template_location ? : self::getOldTemplateLocation();

        $this->reset();
    }

    
    public function setPlainText($plaintext=false)
    {
        $this->plaintext = $plaintext;

        return $this;
    }

    public function setTemplateLocation($template_location)
    {
        $this->template_location = $template_location;

        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    public function setFromEmail($email)
    {
        $this->from_email = $email;

        return $this;
    }

    public function setFromName($name)
    {
        $this->from_name = $name;

        return $this;
    }

    public function setFrom(array $from)
    {
        if (array_key_exists('email', $from) && array_key_exists('name', $from)) {
            $this->from_email = $from['email'];
            $this->from_name = $from['name'];
        } else {
            throw new \Exception('The "from" parameter must be an array and contain the keys "name" and "email".');
        }

        return $this;
    }

    public function addTo($name, $email)
    {
        $this->to[] = array(
            'name' => $name,
            'email' => $email,
        );

        return $this;
    }

    public function setTo(array $to)
    {
        $this->to = $to;

        return $this;
    }

    public function setPlaceholders(array $placeholders)
    {
        $this->placeholders = $placeholders;

        return $this;
    }

    public function addPlaceholder($key, $value)
    {
        $this->placeholders[$key] = $value;

        return $this;
    }

    public function setMergeVars(array $merge_vars)
    {
        $this->merge_vars = $merge_vars;

        return $this;
    }

    public function addMergeVars($key, $value)
    {
        $this->merge_vars[$key] = $value;

        return $this;
    }

    public function setGlobalMergeVars(array $global_merge_vars)
    {
        $this->global_merge_vars = $global_merge_vars;

        return $this;
    }

    public function addGlobalMergeVars($key, $value)
    {
        $this->global_merge_vars[$key] = $value;

        return $this;
    }

    public function setTags(array $tags)
    {
        $this->tags = $tags;

        return $this;
    }

    public function addTag($tag)
    {
        if (!in_array($tag, $this->tags)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function setMergeLanguage($merge_language)
    {
        $this->merge_language = $merge_language;

        return $this;
    }

    public function setAutoText($auto_text)
    {
        $this->auto_text = $auto_text;

        return $this;
    }

    public function setTrackOpens($track_opens)
    {
        $this->track_opens = $track_opens;

        return $this;
    }

    public function setTrackClicks($track_clicks)
    {
        $this->track_clicks = $track_clicks;

        return $this;
    }

    public function setSubaccount($subaccount)
    {
        $this->subaccount = $subaccount;

        return $this;
    }

    public function setGoogleAnalyticsDomains($google_analytics_domains)
    {
        $this->google_analytics_domains = $google_analytics_domains;

        return $this;
    }

    public function setGoogleAnalyticsCampaign(array $google_analytics_campaign)
    {
        $this->google_analytics_campaign = $google_analytics_campaign;

        return $this;
    }

    public function addAttachments($mime, $name, $content)
    {
        $this->attachments[] = array(
            'type' => $mime,
            'name' => $name,
            'content' => $content,
        );
    }

    public function reset()
    {
        $this->template = null;
        $this->subject = null;
        $this->from_email = null;
        $this->from_name = null;
        $this->to = array();
        $this->placeholders = null;
        $this->merge_vars = null;
        $this->tags = null;
        $this->attachments = array();
        
        $this->merge_language = 'handlebars';
        $this->auto_text = false;
        $this->track_opens = true;
        $this->track_clicks = true;
        $this->subaccount = 'skeleton';
    }

    public function sent($reset = true)
    {
        $parameters = $this->getMandrillParameters();

        file_put_contents('/tmp/mandrill_parameters', print_r($parameters, 1));

        $result = $this->mandrill_message->send($parameters);

        if ($reset) {
            $this->reset();
        }

        return $result;
    }

    private function generateHtml()
    {

        if(!$file = $this->plaintext){
         
            if (!$this->template_location || !is_dir($this->template_location)) {
                throw new \Exception('Template location is not a directory: '.$this->template_location);
            }
            $file = @file_get_contents($this->template_location.$this->template);
            if (!$file) {
                throw new \Exception("Template file ".$this->template_location.$this->template.' doesn\'t exist!');
            }
        }

        $patterns = $replacements = array();
        if (is_array($this->placeholders)) {
            $i = 0;
            foreach ($this->placeholders as $key => $value) {
                $patterns[$i] = "/{{{$key}}}/";
                $replacements[$i] = $value;
                $i++;
            }
        }
    
        return preg_replace($patterns, $replacements, $file);
    }

    private function getMandrillParameters()
    {
        $parameters = array(
            'subject' => $this->subject,
            'from_email' => $this->from_email,
            'from_name' => $this->from_name,
            'html' => $this->generateHtml(),
            'to' => $this->to,
            'auto_text' => $this->auto_text,
            'track_opens' => $this->track_opens,
            'track_clicks' => $this->track_clicks,
        );

        if ($this->merge_vars) {
            $parameters['merge_vars'] = $this->merge_vars;
        }

        if ($this->global_merge_vars) {
            $parameters['global_merge_vars'] = $this->global_merge_vars;
        }

        if ($this->merge_language) {
            $parameters['merge_language'] = $this->merge_language;
        }

        if ($this->subaccount) {
            $parameters['subaccount'] = $this->subaccount;
        }

        if ($this->google_analytics_domains) {
            $parameters['google_analytics_domains'] = $this->google_analytics_domains;
        }

        if ($this->google_analytics_campaign) {
            $parameters['google_analytics_campaign'] = $this->google_analytics_campaign;
        }

        if (!empty($this->attachments)) {
            $parameters['attachments'] = $this->attachments;
        }

        return $this->checkMandrillRequiredParameters($parameters);
    }

    /**
     * Check the mandrill mandatory parameter.
     * 
     * @param array $parameters
     * @return array
     */
    private function checkMandrillRequiredParameters($parameters)
    {
        if (!array_key_exists('subject', $parameters) || empty($parameters['subject'])) {
            throw new \Exception('"subject" parameter must be defined');
        }

        if (!array_key_exists('from_email', $parameters) || empty($parameters['html'])) {
            throw new \Exception('"from_email" parameter must be defined');
        }

        if (!array_key_exists('to', $parameters) || empty($parameters['to'])) {
            throw new \Exception('"to" parameter must be defined');
        }

        if (!array_key_exists('html', $parameters) || empty($parameters['html'])) {
            throw new \Exception('Content of the email can not be empty');
        }

        if (!array_key_exists('from_name', $parameters) || empty($parameters['from_name'])) {
            $parameters['from_name'] = $parameters['from_email'];
        }

        return $parameters;
    }

    public function debug($html = true)
    {
        $eol = '<br>';
        if (!$html) {
            $eol = "\n";
        }

        foreach ($this as $key => $value) {
            if (is_object($value)) {
                echo $key." => ".get_class($value);
            } else {
                echo $key." => ".$value;
            }

            echo $eol;
        }
    }

    public function listTemplatesAvailable($html = true)
    {
        if (!is_dir($this->template_location)) {
            throw new \Exception('Template location is not a correct path: '.$this->template_location);
        }

        $eol = '<br>';
        if (!$html) {
            $eol = "\n";
        }

        if ($files = scandir($this->template_location)) {
            foreach ($files as $file) {
                if (!is_dir($file)) {
                    echo $file;
                }

                echo $eol;
            }
        } else {
            throw new \Exception('Template location is not readable: '.$this->template_location);
        }
    }

    public static function getOldTemplateLocation()
    {
        return __DIR__.'/../../../APIs/emails/public/templates/';
    }
}