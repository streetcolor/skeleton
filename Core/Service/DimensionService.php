<?php

namespace Api\Core\Service;

use Silex\Application;

class DimensionService
{
    /** @var \Silex\Application $app */
    private $app;


    /**
     * @param \Silex\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Convert a dimension to a given mesure
     * 
     * @param  int $baseMesure
     * @param  int $toMesure
     * @param  int $w
     * @param  int $h
     * @param  int $l
     * 
     * @return float
     */
    public function handle($baseMesure, $toMesure, $w, $h, $l)
    {
        if ($baseMesure == 1 && $toMesure == 2) {
            // Convert from kilo to pounds
            $w          = round(($w / 2.54), 1);
            $h          = round(($h / 2.54), 1);
            $l          = round(($l / 2.54), 1);
            $dimensions = $w . '" x ' . $h . '"';
            $dimensions = ($l != '0') ? "{$dimensions} x {$l}" : $dimensions;
        } elseif ($baseMesure == 2 && $toMesure == 1) {
            // Convert pounds to kilos
            $w          = round(($w * 2.54), 1);
            $h          = round(($h * 2.54), 1);
            $l          = round(($l * 2.54), 1);
            $dimensions = $w . ' x ' . $h;
            $dimensions = ($l != '0') ? "{$dimensions} x {$l}" : $dimensions;
        } elseif ($baseMesure == $toMesure) {
            if ($baseMesure == 1) {
                $dimensions = $w . ' x ' . $h;
                $dimensions = ($l != '0') ? "{$dimensions} x {$l}" : $dimensions;
            } else {
                $dimensions = $w . '" x ' . $h . '"';
                $dimensions = ($l != '0') ? "{$dimensions} x {$l}" : $dimensions;
            }
        } else {
            $dimensions = $w . ' x ' . $h;
            $dimensions = ($l != '0') ? "{$dimensions} x {$l}" : $dimensions;
        }

        return $dimensions;
    }
}