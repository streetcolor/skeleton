<?php

namespace Api\Core\Service;

use Entity\Image;
use Silex\Application;

class ImageUrlService
{
    /** @var \Silex\Application $app */
    private $app;

    
    /**
     * @param \Silex\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get the storage's url for an image
     * 
     * @param  string $path
     * 
     * @return string
     */
    public function handle($path)
    {
        return getenv('BASE_STORAGE_PATH') . "/{$path}";
    }
}