<?php

namespace Api\Core\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use GuzzleHttp\Client as GuzzleHttp;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Entity\Country;

class LocalizationServiceProvider implements ServiceProviderInterface
{

	public function register(Application $app)
	{
		/*Define default configuration for the register Provider*/
		$app['localization.options.default'] = array(
			'ip'    => null,
			'model' => null,
			'name'  => null
        );

		/*Registrer configuration into the provider*/
		$app['localization.options.initializer'] = $app->protect(function () use ($app) {

 			static $initialized = false;

            if ($initialized && !isset($app['localization.options'])) {
                return;
            }

            $initialized = true;

            if (!isset($app['localization.options.internal']) || isset($app['localization.options']) ) {
            	if(isset($app['localization.options']))
            		$tmp = array_merge($app['localization.options.internal'], $app['localization.options']);
                $app['localization.options.internal'] = array('default' => isset($tmp) ? $tmp : array());
            }

            $tmp = $app['localization.options.internal'];
            foreach ($tmp as $name => &$options) {
                $options = array_replace($app['localization.options.default'], $options);

                if (!isset($app['localization.default'])) {
                    $app['localization.default'] = $name;
                }
            } 
            $app['localization.options'] = $tmp['default'];

            $app['localization.options.internal'] = $tmp['default'];
        });

		$app['localization.ip'] = $app->share(function () use ($app) {

			$app['localization.options.initializer']();
			try {
				$session = $app['session'];
			} catch (\Exception $e) {}

			if ($session->get('localization.ip', false)) {
                $country = $session->get('localization.ip'); 
                if (is_string($country)) {
                    return unserialize($country);
                }
            }

			$gi = geoip_open("/usr/share/GeoIP/GeoIP.dat", GEOIP_STANDARD);
			$countryName = geoip_country_name_by_addr($gi, $app['localization.options.internal']['ip']);

			$model = $app['localization.options.internal']['model'];
			$country = $app[$model]($countryName);

			if (isset($session)) {
				$session->set('localization.ip', serialize($country));
			}

			return $country;

		});

		$app['localization.model.repository'] = $app->protect(function ($countryName) use ($app) {

			$repositoryCountry = $app['orm.em']->getRepository('Entity:Country');

			$getCountry = $repositoryCountry->findOneBy(
				array('country_en' => $countryName)
			);

			return $getCountry;
		});

		$app['localization.id_country'] = $app->share(function () use ($app) {
			$app['localization.options.initializer']();
			static $initialized_id = false;

            if ($initialized_id) {
                return;
            }

            $initialized_id = true;

			if ($id = $app['request']->get($app['localization.options.internal']['name'])) { 
				return $id;
			} elseif ($id = $app['request']->cookies->get($app['localization.options.internal']['name'])) {
				return $id;
			} elseif ($app['service.user']->isCustomer()) {
				$customer = $app['orm.em']->getRepository('Entity:Customer')->find($app['service.user']->getId());
				if ($customer && count($addresses = $customer->getAddresses())) {
					return $addresses[0]->getCountry()->getIdCountry();
				} elseif (count($orders = $customer->getOrders())) { //If there are customer orders
					if ($orders[0]->getShippingCountry()->getIdCountry() != 0)
						return $orders[0]->getShippingCountry()->getCountry()->getIdCountry();

					return $orders[0]->getBillingCountry()->getCountry()->getIdCountry();
				}
			}
			
			$country = $app['localization.ip'];
			if ($country instanceof Country) {
				return  $country->getIdCountry();
			}

			return false;
		});

		$app['localization.cookie'] = $app->protect(function (Response $response, $id_country) use ($app) {
			$app['localization.options.initializer']();
			$cookie =  new Cookie($app['localization.options.internal']['name'], $id_country, time()+3600*24*365);

			$response->headers->setCookie($cookie);

			return $response;
		});

		$app['localization.cookie.reset'] = $app->protect(function (Response $response) use ($app) {
			$app['localization.options.initializer']();

			$response->headers->clearCookie($app['localization.options.internal']['name']);

			return $response;
		});

	}

	public function boot(Application $app)
	{
		if (php_sapi_name() !== 'cli') {
			if(!isset($app['localization.options.default']['ip'])){
				throw new \Exception('IP is not defined');
			}
			if(!isset($app['localization.options.default']['model'])){
				throw new \Exception('Model is not defined');
			}
			if(!isset($app['localization.options.default']['name'])){
				throw new \Exception('Model name is not defined');
			}
		}
	}

}