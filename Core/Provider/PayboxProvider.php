<?php

namespace Api\Core\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

/**
 * PayboxProvider.
 * Paybox integration manual
 * http://www1.paybox.com/wp-content/uploads/2014/06/ManuelIntegrationPayboxDirect_V6.2_FR.pdf
 * 
 * @final
 */
final class PayboxProvider implements ServiceProviderInterface
{
    private $allowed_type = array(
        '00001',
        '00002',
        '00003',
        '00004',
        '00005',
        '00011',
        '00012',
        '00013',
        '00014',
        '00017',
        '00051',
        '00052',
        '00053',
        '00054',
        '00055',
        '00056',
        '00057',
        '00058',
        '00061',
    );

    private $allowed_parameters = array(
        'SITE',
        'RANG',
        'IDENTIFIANT',
        'VERSION',
        'TYPE',
        'DATEQ',
        'NUMQUESTION',
        'CLE',
        'MONTANT',
        'DEVISE',
        'REFERENCE',
        'REFABONNE',
        'REFABONNE',
        'PORTEUR',
        'DATEVAL',
        'CVV',
        'ACTIVITE',
        'ARCHIVAGE',
        'DIFFERE',
        'NUMAPPEL',
        'NUMTRANS',
        'AUTORISATION',
        'PAYS',
        'PRIV_CODETRAITEMENT',
        'DATENAISS',
        'ACQUEREUR',
        'TYPECARTE',
        'SHA-1',
        'ERRORCODETEST',
        'ID3D',
    );

    private $app;
    private $http_client;
    private $http_cookie_session;

    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        $app['paybox.default_options'] = array(
            'url' => '',
            'mpi' => array(),
            'site' => '',
            'rang' => '',
            'identifiant' => '',
            'cle' => '',
            'version' => '',
        );

        $app['paybox.initializer'] = $app->protect(function() use ($app) {
            static $initialized = false;

            if ($initialized) {
                return;
            }

            $initialized = true;


            if (empty($app['paybox.options'])) {
                throw new \Exception('paybox.options is mandatory.');
            }

            foreach ($app['paybox.default_options'] as $key => $value) {
                if (empty($app['paybox.options'][$key])) {
                    throw new \Exception('"'.$key.'" options is mandatory.');
                }
            }

            $tmp = array_replace($app['paybox.default_options'], $app['paybox.options']);
            $app['paybox.options'] = $tmp;
            $this->app = $app;
            $this->http_client = new Client();
            $this->http_cookie_session = new CookieJar();
        });

        /**
         * Make a paybox refund
         */
        $app['paybox.refund_transaction'] = $app->protect(function(array $parameters) use ($app) {
            $app['paybox.initializer']();
            $parameters['TYPE'] = '00014';

            if ($this->checkPayboxParameters($parameters)) {
                // Allow refund
                return $this->makeRequest($parameters);
            }
        });

        /**
         * Make a transaction
         */
        $app['paybox.make_transaction'] = $app->protect(function(array $parameters) use ($app) {
            $app['paybox.initializer']();
            $parameters['TYPE'] = '00003';

            if ($this->checkPayboxParameters($parameters)) {
                return $this->makeRequest($parameters);
            }
        });

        /**
         * Update amount transaction
         */
        $app['paybox.update_amount_transaction'] = $app->protect(function(array $parameters) use ($app) {
            $app['paybox.initializer']();
            $parameters['TYPE'] = '00013';

            if ($this->checkPayboxParameters($parameters)) {
                return $this->makeRequest($parameters);
            }
        });

        /**
         * Cancel a transaction
         */
        $app['paybox.cancel_transaction'] = $app->protect(function(array $parameters) use ($app) {
            $app['paybox.initializer']();
            $parameters['TYPE'] = '00005';

            if ($this->checkPayboxParameters($parameters)) {
                // Cancel transaction
                return $this->makeRequest($parameters);
            }
        });

        /**
         * Get transaction from paybox
         */
        $app['paybox.get_transaction'] = $app->protect(function(array $parameters) use ($app) {
            $app['paybox.initializer']();
            $parameters['TYPE'] = '00017';

            if ($this->checkPayboxParameters($parameters)) {
                // Get transaction information from paybox
                return $this->makeRequest($parameters);
            }
        });
    }

    public function boot(Application $app)
    {
        //
    }

    /**
     * Check the parameters that will be sent to paybox
     */
    private function checkPayboxParameters(&$parameters)
    {
        // Set default parameters before check it.
        $this->setDefaultParameters($parameters);

        if (empty($parameters['TYPE'])) {
            throw new \Exception('Paybox parameter type is required.');
        } else if (!in_array($parameters['TYPE'], $this->allowed_type)) {
            throw new \Exception('Paybox type '.$parameters['TYPE'].' is not allowed.');
        }

        foreach ($parameters as $key => $value) {
            if (!in_array($key, $this->allowed_parameters)) {
                throw new \Exception('Parameter "'.$key.'" is not allowed.');
            }
        }

        foreach ($this->getPayboxMandatoryParametersForType($parameters['TYPE']) as $mandatory_parameter) {
            if (!array_key_exists($mandatory_parameter, $parameters)) {
                throw new \Exception('Parameter "'.$mandatory_parameter.'" is missing.');
            }
        }

        return true;
    }

    /**
     * Set default parameters that will be sent to paybox
     */
    private function setDefaultParameters(&$parameters)
    {
        // Set the provider options required for paybox request
        foreach ($this->app['paybox.options'] as $key => $value) {
            $key = strtoupper($key);
            if (!array_key_exists($key, $parameters) && in_array($key, $this->allowed_parameters)) {
                $parameters[strtoupper($key)] = $value;
            }
        }
        
        if (!array_key_exists('DEVISE', $parameters)) {
            $parameters['DEVISE'] = '978';
        }

        if (!array_key_exists('DATEQ', $parameters)) {
            $parameters['DATEQ'] = (new \DateTime())->format('dmYHis');
        }

        if (!array_key_exists('NUMQUESTION', $parameters)) {
            $parameters['NUMQUESTION'] = time();
        }
    }

    /**
     * Get the mandatory parameters for specific paybox request type
     */
    private function getPayboxMandatoryParametersForType($type)
    {
        $mandatory_parameters = array(
            'GENERAL' => array(
                'SITE',
                'RANG',
                'IDENTIFIANT',
                'VERSION',
                'TYPE',
                'DATEQ',
                'NUMQUESTION',
                'CLE',
            ),
            '00001' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00002' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'NUMAPPEL',
                'NUMTRANS',
            ),
            '00003' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00004' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00005' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'NUMAPPEL',
                'NUMTRANS',
            ),
            '00011' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
            ),
            '00012' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00013' => array(
                'MONTANT',
                'DEVISE',
                'NUMAPPEL',
                'NUMTRANS',
            ),
            '00014' => array(
                'MONTANT',
                'DEVISE',
                'NUMAPPEL',
                'NUMTRANS',
            ),
            '00017' => array(
                'NUMTRANS',
            ),
            '00051' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'REFABONNE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00052' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'REFABONNE',
                'NUMAPPEL',
                'NUMTRANS',
            ),
            '00053' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'REFABONNE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00054' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'REFABONNE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00055' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'REFABONNE',
                'PORTEUR',
                'DATEVAL',
                'NUMAPPEL',
                'NUMTRANS',
            ),
            '00056' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'REFABONNE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00057' => array(
                'MONTANT',
                'DEVISE',
                'REFABONNE',
                'PORTEUR',
                'DATEVAL',
            ),
            '00058' => array(
            ),
            '00061' => array(
                'MONTANT',
                'DEVISE',
                'REFERENCE',
                'REFABONNE',
                'PORTEUR',
                'DATEVAL',
            ),
        );

        return array_merge($mandatory_parameters['GENERAL'], $mandatory_parameters[$type]);
    }

    private function makeRequest($body, $end_point = '', $cookie = true)
    {
        $params = array(
            'headers' => array(
                'content-type' => 'application/x-www-form-urlencoded; charset=utf-8',
            ),
            'cookies' => $this->http_cookie_session,
            'body' => $body,
        );

        $response = $this->http_client->post($this->app['paybox.options']['url'].$end_point, $params);
        $body = $response->getBody()->getContents();
        
        $data = array();
        parse_str($body, $data);

        // Convert data to UTF-8
        foreach ($data as $key => &$value) {
            $value = utf8_encode($value);
        }

        return $data;
    }
}