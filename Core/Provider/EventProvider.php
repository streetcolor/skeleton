<?php

namespace Api\Core\Provider;

use Closure;
use Exception;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Inject all global events.
 *
 * @final
 */
final class EventProvider implements ServiceProviderInterface
{   
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        // 
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     * 
     * @param Silex\Application $app
     */
    public function boot(Application $app)
    {
        $app->before(function(Request $request, Application $app) {
            $response = $this->lunchEvents($request, null, $app);

            if ($response instanceof Response) {
                return $response;
            }
        });

        $app->after(function (Request $request, Response $response) use ($app) {
            $response = $this->lunchEvents($request, $response, $app);

            if ($response instanceof Response) {
                return $response;
            }
        });
    }

    /**
     * Retrieves all events and handles them.
     * 
     * @param  \Symfony\Component\HttpFoundation\Request  $request
     * @param  \Symfony\Component\HttpFoundation\Response $response|null
     * @param  \Silex\Application $app
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function lunchEvents(Request $request, $response = null, Application $app)
    {
        $type = ($response instanceof Response ? "after" : "before");

        $route      = $request->get('_route');
        $controller = $this->retrieveBundle($request);

        // When no '/' provided at the end of the uri, $controller is null
        // but Silex redirects you to the same url adding the '/'
        if ($controller) {
            if (!empty($app["bundle.{$controller}.events"][$type])) {
                foreach ($app["bundle.{$controller}.events"][$type] as $key => $service) {
                    if (is_numeric($key) || ($route === $key)) {
                        $r = $service->handle($request, $response, $app);

                        if ($r instanceof Response) {
                            return $r;
                        }
                    }
                }
            }
        }
    }

    /**
     * Retrieve the bundle's name.
     * 
     * @param  \Symfony\Component\HttpFoundation\Request $request
     * 
     * @return string|null
     */
    private function retrieveBundle(Request $request)
    {
        if ($request->get('_controller') instanceof Closure) {
            return null;
        } else {
            return preg_replace_callback('/^([\w]+)\..+/', function ($matches) {
                return $matches[1];
            }, $request->get('_controller'));
        }
    }
}