<?php

namespace Api\Core\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Entity\User; 
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
/**
 * Acme Service Provider.
 *
 * @package default
 */
class UserProvider implements ServiceProviderInterface, UserProviderInterface
{   

    private $em;


    public function register(Application $app)
    {

        $app['service.security'] = $app->share(function() use ($app) {
            return array(
                'security.firewalls' => array(
                    'secured' => array(
                        'pattern' => '^.*$',
                        'anonymous' => true, // Indispensable car la zone de login se trouve dans la zone sécurisée (tout le front-office)
                        'form' => array(
                                'login_path' => '/user/sign-up', 
                                'check_path' => 'connexion',
                                'default_target_path' => null
                                ),
                        'logout' => array('logout_path' => '/deconnexion'), // url à appeler pour se déconnecter
                        'users' =>  $this,
                        'remember_me' => array(
                            'key'                => 'Choose_A_Unique_Random_Key',
                            'always_remember_me' => false,
                            /* Other options */
                        ),
                    ),
                ),
                'security.access_rules' => array(
                    // ROLE_USER est défini arbitrairement, vous pouvez le remplacer par le nom que vous voulez
                    array('^/account', 'ROLE_USER'),
                    array('^/user/login', 'ROLE_USER'),
                )
        
            );
        });

        /*Use to protect road from within the controller*/
        $app['security.is_granted'] = $app->protect(function(array $roles) use ($app) {

            if(!$app['security.authorization_checker']->isGranted($roles)){
                throw new AccessDeniedHttpException("Access Denied.");
            }

        });
    }
    


    public function loadUserByUsername($username)
    {
        $getUser = $this->em->getRepository('Entity\User')->findOneBy(array('username'=>$username));
        if($getUser){
           return  $getUser;
        }

         throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );

    }

    public function refreshUser(UserInterface $user)
    {
        //echo $user->getUsername(); die; 
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Entity\User';
    }


    public function boot(Application $app)
    {

        $this->em = $app['orm.em'];
         
    }


}
