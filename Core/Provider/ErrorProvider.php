<?php

namespace Api\Core\Provider;

use Exception;
use Api\Core\Service\ErrorService;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * Register all errors services.
 * 
 * @final
 */
final class ErrorProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        $app['service.error'] = $app->protect(function (Exception $e, $code) use ($app) {
            $service = new ErrorService($app);

            $service->handle($e, $code);
        });
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     * 
     * @param Silex\Application $app
     */
    public function boot(Application $app)
    {
        // 
    }
}