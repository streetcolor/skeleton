<?php

namespace Api\Core\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * Register the ORM.
 * 
 * @final
 */
final class OrmProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        $entities   = isset($app['orm.em.options.mappings.entities']) ? $app['orm.em.options.mappings.entities'] : [];
        $entities[] = [
            'type'      => 'annotation',
            'namespace' => 'Entity',
            'path'      => __DIR__ . '/../../../Model/Entity',
            'alias'     => 'Entity',
        ];

        $app['orm.em.options.mappings.entities'] = $entities;
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     * 
     * @param Silex\Application $app
     */
    public function boot(Application $app)
    {
        // 
    }
}