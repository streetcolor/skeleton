<?php

namespace Api\Core\Provider;

use Exception;
use OpenCloud\ObjectStore\Exception\ObjectNotFoundException;
use OpenCloud\ObjectStore\Resource\Container;
use OpenCloud\OpenStack;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @final
 */
final class ImageProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        $app['service.image.openCloud'] = $app->share(function () use ($app) {
            $client = new OpenStack($app['openCloud.options']['url'], [
                'username'   => $app['openCloud.options']['username'],
                'password'   => $app['openCloud.options']['password'],
                'tenantName' => $app['openCloud.options']['tenantName'],
            ]);

            $objectStore = $client->objectStoreService('swift', $app['openCloud.options']['datacenter']);
            $container   = $objectStore->getContainer($app['openCloud.options']['container'] );

            return $container;
        });

        $app['service.image.upload.basic'] = $app->protect(
            function (
                Container $container,
                $imagePath,
                $remotePath
            ) use ($app) {
                try {


                   $result = $container->uploadObject($remotePath, fopen($imagePath, 'r+'));

                    if ($result->getName()) {
                        unlink($imagePath);

                        $path = $result->getName(); 
                        $path = explode('/', $result->getName());
    

                        return $path[2];

                    }
                    return true;
                } catch (ObjectNotFoundException $e) {
                    throw new Exception($e->getMessage());
                }
            }
        );

        $app['service.image.upload'] = $app->protect(
            function (
                Container $container,
                $imagePath,
                $remotePath,
                $format,
                $width,
                $height
            ) use ($app) {

                $destinationPath = dirname($app['request']->server->get('DOCUMENT_ROOT'))
                    . '/resource/uploads/temp/'
                    . basename($imagePath)
                    . '_'
                    . date('Y-m-d_His');

                if (!file_exists($imagePath)) {
                    throw new \Exception("ERROR - No file exist");
                }
                
                $imageSize = getimagesize($imagePath);

                $crop       = '-crop';
                $heightAuto = false;
                $shrink     = '';
                $resize     = '';

                switch ($format) {
                    case 'm':
                        $crop   = '';
                        $shrink = '>';
                        $resize = ' -resize';
                        break;

                    case 'l':
                        $width  = $imageSize[0];
                        $height = $imageSize[1];
                        $crop   = '';
                        break;

                    case 'grid':
                        $height     = $imageSize[1];
                        $crop       = '';
                        $heightAuto = true;
                        break;

                    case 'p':
                        break;
                }

                echo exec('convert ' . $imagePath . ' -thumbnail "' . $width . ($heightAuto ? '' : 'X' . $height . ($shrink ? $shrink : '^')) . '" -gravity center ' . $crop . ' "' . $width . ($heightAuto ? '' : 'X' . $height) . $shrink . '+0+0" +repage ' . $destinationPath);

                $result = $container->uploadObject($remotePath, fopen($destinationPath, 'r+'));

                if ($result->getName()) {
                    unlink($destinationPath);

                    $path = $result->getName(); 
                    $path = explode('/', $result->getName());

                    return $path[2];

                }

                return false;
            }
        );

        $app['service.image.resize'] = $app->protect(
            function (
                Container $container,
                $remote_image,
                $remote_destination,
                $width,
                $height
            ) use ($app) {
               try {
                    $file = $container->getObject($remote_image);

                    $objectContent = $file->getContent();
                    $objectContent->rewind();

                    $stream        = $objectContent->getStream();
                    $localFilename = tempnam("/tmp", 'php-opencloud-');

                    if (!$file->getName()) {
                        throw new \OpenCloud\Common\Exceptions\IOError('No object found in the container');
                    }

                    //$retval = fwrite($fp, $o->getContent());

                    if (file_put_contents($localFilename, $stream) === FALSE) {
                      throw new Exception("ERROR - Cannot write to file ($temp)");
                    } else {
                        echo exec('convert '.$localFilename.'  -thumbnail "'.$width.'X'.$height.'^" -gravity center -crop '.$width.'X'.$height.'+0+0 +repage '.$localFilename);

                        if (file_exists($localFilename)) {
                            $result = $container->uploadObject($remote_destination, fopen($localFilename, 'r+'));

                            if ($result->getName()) {
                                $path = $result->getName(); 
                                $path = explode('/', $path);

                                return end($path);
                            }

                            return false;
                        }
                    }
                } catch (ObjectNotFoundException $e) {
                    throw new Exception($remote_image." does not exist!");
                }
            }
        );

        $app['service.image.copy'] = $app->protect(
            function (
                Container $container,
                $imagePath,
                $remotePath
            ) use ($app) {
                try {
                    $object = $container->getObject($imagePath);
                    $result = $object->copy($app['openCloud.options']['container']. '/' .$remotePath);

                    if ($result->getStatusCode() == 201) {
                        return $remotePath;
                    }

                    return false;
                } catch (ObjectNotFoundException $e) {
                    throw new Exception($e->getMessage());
                }
            }
        );

        $app['service.image.delete'] = $app->protect(
            function (
                Container $container,
                $imagePath
            ) use ($app) { 
                $object = $container->getObject($imagePath);
                $result = $object->delete();

                if ($result->getStatusCode() == 201) {
                    return true;
                }

                return false;
            }
        );
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     * 
     * @param Silex\Application $app
     */
    public function boot(Application $app)
    {
        // 
    }
}