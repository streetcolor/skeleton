<?php

namespace Api\Core\Provider;

use Api\Core\Service\ImageUrlService;
use Api\Core\Service\MandrillMailerService;
use Api\Core\Service\HttpClientService;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Validation\Validator;
use GuzzleHttp\Client;

/**
 * Register all global services.
 *
 * @final
 */
final class AppProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        $app['service.image.getUrl'] = $app->protect(function ($path) use ($app) {
            $service = new ImageUrlService($app);

            return $service->handle($path);
        });

        $app['service.validator'] = $app->share(function($app) {
            return new Validator($app['validator']);
        });

        $app['service.mailer'] = $app->share(function($template_location = null) {
            $mandrill_api_key = getenv('MANDRILL_API_KEY');
            return new MandrillMailerService($mandrill_api_key, $template_location);
        });

        $app['service.http.client'] = $app->protect(function($endpoint=null, $method=null, $type = null) {
             $client = new HttpClientService($method, $endpoint ,$type);
             return $client->handle(); 
        });
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     * 
     * @param Silex\Application $app
     */
    public function boot(Application $app)
    {
        // 
    }
}