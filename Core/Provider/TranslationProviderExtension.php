<?php

namespace Api\Core\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Translation\Loader\XliffFileLoader;

/**
 * Register the translation extension.
 *
 * @final
 */
final class TranslationProviderExtension implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        $app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
            $translator->addLoader('xliff', new XliffFileLoader());
            $translator->addResource('xliff', __DIR__ . '/../../translation/en.xliff', 'en');
            $translator->addResource('xliff', __DIR__ . '/../../translation/fr.xliff', 'fr');

            $translator->setLocale($app['session']->get('langue', 'fr'));

            return $translator;
        }));
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     * 
     * @param Silex\Application $app
     */
    public function boot(Application $app)
    {
        // 
    }
}