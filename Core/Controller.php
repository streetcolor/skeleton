<?php

namespace Api\Core;

use Silex\Application;
use Api\Core\Service\MandrillMailerService;

/**
 * Base Controller.
 *
 * @abstract
 */
abstract class Controller
{
    /** @var \Silex\Application The application's instance */
    protected $app;

    /** @var \Symfony\Component\HttpFoundation\Request The Request's instance */
    protected $request;


    /**
     * @param \Silex\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app        = $app;
        $this->request    = $this->app['request'];
        $this->queries    = $this->request->query;
        $this->body       = $this->request->request;
        $this->headers    = $this->request->headers;
        $this->files      = $this->request->files;
        $this->attributes = $this->request->attributes;
        $this->cookies    = $this->request->cookies;
        $this->mailer     = $this->app['service.mailer'];
    }
}