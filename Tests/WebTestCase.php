<?php

namespace Api\Tests;

use Silex\WebTestCase as DefaultWebTestCase;

class WebTestCase extends DefaultWebTestCase
{
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap.php';

        return $app;
    }
}
