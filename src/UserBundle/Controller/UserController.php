<?php
namespace Api\Src\UserBundle\Controller;

use Api\Core\Controller;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Entity\User; 
/**
 * Deafault controller
 *
 * @package default
 */
class UserController extends Controller
{

    public function registerAction(Request $request){


        $error = [];
        $data  = $request->request->all();
        $count = 0;

        $validator = $this->app['service.validator'];
        $encoder   = new MessageDigestPasswordEncoder();
        $user      = new User();

        $data = [
            'password' => '1111',
            'lastname' => '1111',
            'firstname' => '1111',
            'username' => '1111'
        ];

        $password  = ($data['password']) ? $encoder->encodePassword($data['password'], '') : false;
        $user->setLastname($data['lastname']);
        $user->setFirstname($data['firstname']);
        $user->setUsername($data['username']);
        $user->setPassword($password);
        
        if (!$validator->validate($user, ['insert'])) {
            $error = $validator->getErrors();
        }
        else{
            $this->app['orm.em']->persist($user);
            $this->app['orm.em']->flush();    
            $token = new UsernamePasswordToken($user, $user->getPassword(), 'secured', array('ROLE_USER'));
            
            if($token){
                $this->app['security.token_storage']->setToken($token);
                $data = [
                    'id_user'  => $token->getUser()->getIdUser(),
                    'username' => $token->getUser()->getUsername(),
                    'role'     => $token->getUser()->getRoles()
                ];
                $count =1;                
            }

        }
        
        return $this->app->json(
            [
                'error' => $error,
                'data'  => $data,
            ], 
            200, 
            [
                'X-Total-Count' => $count
            ]
        );
        
    }

    public function LoginAction(Request $request){

        $token = $this->app['security.token_storage']->getToken();

        if($this->app['security.authorization_checker']->isGranted('ROLE_USER')){
            
            if (null !== $token) {
                
                return $this->app->json(
                    [
                        'error' => null,
                        'data'  => [
                                'username' => $token->getUser()->getUsername(),
                                'id_user'  => $token->getUser()->getIdUser(),
                                'role'     => $token->getUser()->getRoles()
                            ],
                    ], 
                    200, 
                    [
                        'X-Total-Count' => 1
                    ]
                );
            }
    
        }
        
    }
}
