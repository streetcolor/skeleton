<?php

namespace Api\Src\UserBundle;

use Api\Core\Bundle;
use Silex\Application;

/**
 * OrderBundle
 *
 * @package default
 */
class UserBundle extends Bundle
{
    /** @var array All events related to the bundle */
    protected $events = [
        'before' => [
            
        ],
        'after' => [
            
        ],
    ];

    /** @var array All providers related to the bundle */
    protected $providers = [
    ];

    /**
     * Configure the Api
     *
     * @return array
     */
    public function config()
    {
    }


    /**
     * Connect routes to their controllers
     *
     * @param  \Silex\Application $app
     * 
     * @return object
     */
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        
        $controllers
            ->post('/register', 'user.controller:registerAction')
            ->bind('user_signup_post')
        ;
    
        $controllers
            ->get('/login', 'user.controller:loginAction')
            ->bind('user_login')
            ->before(function() use ($app) {
                return $app['security.is_granted'](['ROLE_USER']);
            });
        ;
    
        return $controllers;
    }
}