#!/usr/bin/env php5

<?php

use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Symfony\Component\Console\Helper\HelperSet;

// Load your bootstrap or instantiate application and setup your config here
if (!defined('APP_ROOT')) {
    define('APP_ROOT', __DIR__ . '/../../');
}

require_once APP_ROOT . '/vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(APP_ROOT);
$dotenv->load();

$app = new Silex\Application();
 
// Doctrine DBAL
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => [
        'driver'   => 'pdo_mysql',
        'host'     => getenv('DB_HOST'),
        'dbname'   => getenv('DB_NAME'),
        'user'     => getenv('DB_USER'),
        'password' => getenv('DB_PASS'),
        'charset'  => 'utf8',
    ],
));

 
// Doctrine ORM, I like the defaults, so I've only modified the proxies dir and entities path / namespace
$app->register(new Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), [
    'orm.proxies_dir' => APP_ROOT . '/cache/doctrine/proxy',
    'orm.em.options'  => [
        'mappings' => [
            // Using actual filesystem paths
            [
                'type'      => 'annotation',
                'namespace' => 'Entity',
                'path'      => APP_ROOT . '/../Model/Entity',
            ],
        ],
    ],
]);
 
$helperSet = new HelperSet([
    'db' => new ConnectionHelper($app['orm.em']->getConnection()),
    'em' => new EntityManagerHelper($app['orm.em'])
]);
 
/**
 * @notice php src/HomeBundle/console.php orm:generate-entities ../Model
 * @notice php src/HomeBundle/console.php orm:schema-tool:update
 */
ConsoleRunner::run($helperSet);