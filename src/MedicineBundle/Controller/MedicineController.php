<?php

namespace Api\Src\MedicineBundle\Controller;

use Api\Core\Controller;
use Silex\Application;

/**
 * @prefix('/medicine/')
 * 
 * @final
 * @package default
 */
final class MedicineController extends Controller
{
    /**
     * Get an medicine by ID
     */
    public function get($id_medicine=null)
    {
        $em = $this->app['orm.em'];

        $medicines = $em->getRepository('Entity:Medicine')->findAll();

        $return = [];
        foreach ($medicines as $key => $medicine) {
            $return[] = $medicine->serialize();
        }

        return $this->app->json($return, 200, [
            'X-Total-Count' => count($medicines)
        ]);
    }

}