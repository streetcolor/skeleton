<?php

namespace Api\Src\MedicineBundle;

use Api\Core\Bundle;
use Silex\Application;

/**
 * OrderBundle
 *
 * @package default
 */
class MedicineBundle extends Bundle
{
    /** @var array All events related to the bundle */
    protected $events = [
        'before' => [
            
        ],
        'after' => [
            
        ],
    ];

    /** @var array All providers related to the bundle */
    protected $providers = [
        [
        ],
    ];

    /**
     * Configure the Api
     *
     * @return array
     */
    public function config()
    {
    
    }


    /**
     * Connect routes to their controllers
     *
     * @param  \Silex\Application $app
     * 
     * @return object
     */
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        
        $controllers->get('/all', 'medicine.controller:get')
            ->bind('medicine_get_all');
    
        return $controllers;
    }
}