<?php

namespace Api\Src\MedicineBundle\Provider;

use Silex\Provider\ValidatorServiceProvider;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Saxulum\DoctrineOrmManagerRegistry\Silex\Provider\DoctrineOrmManagerRegistryProvider;
use Validation\Constraint\Medicine\OneOfThemValidator;

/**
 * Acme Service Provider.
 *
 * @package default
 */
class MedicineConstraintProvider implements ServiceProviderInterface
{
    /**
     * Register bindings in the container.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        /*
         * Medicine validator
         * @note: uses ValidatorServiceProvider
         */
        $app['medicine.validator'] = $app->share(function() use ($app) {
            return new Core\Validation\Validator($app['validator']);
        });

        $app->register(new ValidatorServiceProvider, [
            'validator.validator_service_ids' => [
                'validator.oneOfThem'      => 'validator.constraint.oneOfThem',
            ],
        ]);
        $app['validator.constraint.oneOfThem'] = $app->share(function($app) {
            return new OneOfThemValidator($app['request'], $app['orm.em']);
        });
     

        $app->register(new DoctrineOrmManagerRegistryProvider());
    }

    /**
     * Register any other things in our application.
     * 
     * @param Silex\Application $app
     */
    public function boot(Application $app)
    {
        // 
    }
}