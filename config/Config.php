<?php

namespace Api\Config;

use Api\Core\Provider\AlgoliaProvider;
use Api\Core\Provider\AppProvider;
use Api\Core\Provider\ErrorProvider;
use Api\Core\Provider\EventProvider;
use Api\Core\Provider\FirewallProvider;
use Api\Core\Provider\ImageProvider;
use Api\Core\Provider\IntercomProvider;
use Api\Core\Provider\OrmProvider;
use Api\Core\Provider\PayboxProvider;
use Api\Core\Provider\TranslationProviderExtension;
use Api\Core\Provider\UserProvider;
use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\RememberMeServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;

/**
 * Register all the application's services.
 *
 * ALL the environment classes MUST inherit this base class.
 *
 * @abstract
 */
abstract class Config
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        /**
         * Custom error provider.
         */
        $app->register(new ErrorProvider);

        /**
         * Controllers as a Service.
         *
         * @note Silex Native
         * @doc http://silex.sensiolabs.org/doc/providers/service_controller.html
         */
        $app->register(new ServiceControllerServiceProvider);


        /**
         * Custom user provider.
         */
        $app->register(new UserProvider);

        /**
         * Custom orm provider.
         */
        $app->register(new OrmProvider);

        /**
         * Doctrine DBAL.
         *
         * @note Silex Native
         * @doc http://silex.sensiolabs.org/doc/providers/doctrine.html
         */
        $app->register(new DoctrineServiceProvider, [
            'db.options' => [
                'driver'   => 'pdo_mysql',
                'host'     => 'localhost',//getenv('DB_HOST'),
                'dbname'   => 'GWT',//getenv('DB_NAME'),
                'user'     => 'streetcolor',//getenv('DB_USER'),
                'password' => 'viveinternet9452',//getenv('DB_PASS'),
                'charset'  => 'utf8',
            ],
        ]);
        
        /**
         * Doctrine ORM.
         */
        $app->register(new DoctrineOrmServiceProvider, [
            'orm.proxies_dir' => getenv('DOCTRINE_PROXIES_DIR'),
            'orm.em.options'  => [
                'mappings' => $app['orm.em.options.mappings.entities'],
            ],
            'orm.custom.functions.string' => [
                'IF'           => '\DoctrineExtensions\Query\Mysql\IfElse',
                'IFNULL'       => '\DoctrineExtensions\Query\Mysql\IfNull',
                'NULLIF'       => '\DoctrineExtensions\Query\Mysql\NullIf',
            ]
        ]);

        /**
         * Session.
         *
         * @note Silex Native
         * @doc http://silex.sensiolabs.org/doc/providers/session.html
         */
        $app->register(new SessionServiceProvider, array(
            'session.test' => false !== getenv('TEST')
        ));
        
        /**
         * Url Generator.
         *
         * @note Silex Native
         * @doc http://silex.sensiolabs.org/doc/providers/url_generator.html
         */
        $app->register(new UrlGeneratorServiceProvider);

        /**
         * Translation.
         *
         * @note Silex Native
         * @doc http://silex.sensiolabs.org/doc/providers/translation.html
         */
        $app->register(new TranslationServiceProvider(), [
            'locale_fallbacks' => [getenv('APP_FALLBACK_LANG')]
        ]);

        /**
         * Custom translation extension.
         */
        $app->register(new TranslationProviderExtension);

        /**
         * Fragment Provider.
         *
         * @note Silex Native
         * @doc http://silex.sensiolabs.org/doc/providers/http_fragment.html
         */
        $app->register(new HttpFragmentServiceProvider);

        /**
         * Custom app provider.
         */
        $app->register(new AppProvider);

        /**
         * Custom event provider.
         */
        $app->register(new EventProvider);

        // $app->register(new AlgoliaProvider, [
        //     'algolia.options' => [
        //         'id'  => getenv('ALGOLIA_APPLICATION_ID'),
        //         'key' => getenv('ALGOLIA_API_KEY'),
        //     ],
        // ]);

        // $app->register(new IntercomProvider, [
        //     'intercom.options' => [
        //         'id'  => getenv('INTERCOM_APPLICATION_ID'),
        //         'key' => getenv('INTERCOM_API_KEY'),
        //     ],
        // ]);

        // $app->register(new ImageProvider, [
        //     'openCloud.options' => [
        //         'url'        => getenv('OPENCLOUD_URL'),
        //         'username'   => getenv('OPENCLOUD_USERNAME'),
        //         'password'   => getenv('OPENCLOUD_PASSWORD'),
        //         'tenantName' => getenv('OPENCLOUD_TENANTNAME'),
        //         'datacenter' => getenv('OPENCLOUD_DATACENTER'),
        //         'container'  => getenv('OPENCLOUD_CONTAINER'),
        //     ],
        // ]);


        // $app->register(new PayboxProvider, array(
        //     'paybox.options' => array(
        //         'url' => getenv('PAYBOX_URL'),
        //         'mpi' => getenv('PAYBOX_MPI'),
        //         'site' => getenv('PAYBOX_SITE'),
        //         'rang' => getenv('PAYBOX_RANG'),
        //         'identifiant' => getenv('PAYBOX_IDENTIFIANT'),
        //         'cle' => getenv('PAYBOX_CLE'),
        //         'version' => getenv('PAYBOX_VERSION'),
        //     )
        // ));

        $app->register(new ValidatorServiceProvider());


        $app->register(new UserProvider());

        /**
        * Security provider
        *
        * @note Silex Native
        * @note Please to define user.provider 
        * @doc http://silex.sensiolabs.org/doc/providers/security.html
        */
        $app->register(new SecurityServiceProvider(), $app['service.security']);

        /**
        * Security Provider
        *
        * @note Silex Native
        * @note Please to define user.provider 
        * @doc http://silex.sensiolabs.org/doc/providers/remember_me.html
        */
        $app->register(new RememberMeServiceProvider());
    }
}