<?php

namespace Api\Config\Env;

use Api\Config\Config;
use Silex\Application;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Symfony\Component\Debug\Debug;

/**
 * DEVELOPMENT specific configuration class
 *
 * We register here all the required Providers.
 * DO NOT extend or register simple services here,
 * create instead a Service Provider and register it here.
<<<<<<< HEAD
=======
 *
>>>>>>> dev
 */
class Dev extends Config
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        parent::register($app);

        Debug::enable();

        $app['debug'] = true;

        /**
         * Web profiler
         *
         * @note Silex native
         * @doc https://github.com/silexphp/Silex-WebProfiler
         */
        // $app->register(new WebProfilerServiceProvider, [
        //     'profiler.cache_dir' => getenv('PROFILER_CACHE_DIR'),
        // ]);

        /**
         * Monolog
         *
         * @note Silex Native
         * + we MUST use symfony http-kernel 2.8 in order to make
         * this provider work.
         * @doc http://silex.sensiolabs.org/doc/providers/monolog.html
         * @see https://github.com/silexphp/Silex-Skeleton/issues/60
         */
        $app->register(new MonologServiceProvider, [
            'monolog.logfile' => getenv('MONOLOG_LOGFILE'),
        ]);
    }
}