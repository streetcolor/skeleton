<?php

namespace Api\Config\Env;

use Api\Config\Config;
use Silex\Application;
use Silex\Provider\WebProfilerServiceProvider;

/**
 * PRODUCTION specific configuration class
 *
 * We register here all the required Providers.
 * DO NOT extend or register simple services here,
 * create instead a Service Provider and register it here.
 *
 * @final
 */
final class Prod extends Config
{
    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     * 
     * @param Silex\Application $app
     */
    public function register(Application $app)
    {
        parent::register($app);

        $app['debug'] = false;
    }
}